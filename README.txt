INTRODUCTION
------------
We all agree that spammers don't use spaces in their usernames, right?
This module forces users to use spaces in their usernames so that
spammers can't register (until they figure out a way to).

MAINTAINERS
-----------
Current maintainers:
 * Kartagis - https://www.drupal.org/u/kartagis
